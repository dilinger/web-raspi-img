---
layout: page
title: Tested images
date: 2020-07-07 01:06:23 -0500
permalink: /tested-images/
---

|----------------|------------|---------------------|-------------------------------------------------------------------|---------------------------------------------------------------------|
| **Build date** | **Family** | **Tested hardware** | **File links**                                                    | **Notes**                                                           |
|----------------|------------|---------------------|-------------------------------------------------------------------|---------------------------------------------------------------------|
| 2020.05.01     | 3          | 3B+                 | [xz-compressed image](/verified/20200501_raspi_3.img.xz)          | Tested: Boots OK, HDMI, USB keyboard, wired and wireless networking |
|                |            |                     | [sha256sum](/verified/20200501_raspi_3.xz.sha256)                 |                                                                     |
|                |            |                     | [GPG-signed sha256sum](/verified/20200501_raspi_3.xz.sha256.asc)  |                                                                     |
|----------------|------------|---------------------|-------------------------------------------------------------------|---------------------------------------------------------------------|
| 2020.05.05     | 0/1        | early 1B (256MB)    | [xz-compressed image](/verified/20200505_raspi_0w.img.xz)         | Tested: Boots OK, HDMI, USB keyboard, wired networking              |
|                |            |                     | [sha256sum](/verified/20200505_raspi_0w.xz.sha256)                |                                                                     |
|                |            |                     | [GPG-signed sha256sum](/verified/20200505_raspi_0w.xz.sha256.asc) |                                                                     |
|----------------|------------|---------------------|-------------------------------------------------------------------|---------------------------------------------------------------------|
| 2020.05.05     | 0/1        | 0W                  | [xz-compressed image](/verified/20200505_raspi_0w.img.xz)         | Tested: Boots OK, HDMI, USB keyboard, wireless networking           |
|                |            |                     | [sha256sum](/verified/20200505_raspi_0w.xz.sha256)                |                                                                     |
|                |            |                     | [GPG-signed sha256sum](/verified/20200505_raspi_0w.xz.sha256.asc) |                                                                     |
|----------------|------------|---------------------|-------------------------------------------------------------------|---------------------------------------------------------------------|
| 2020.05.12     | 2          | 2B                  | [xz-compressed image](/verified/20200512_raspi_2.img.xz)          | Tested: Boots OK, HDMI, USB keyboard, wired networking              |
|                |            |                     | [sha256sum](/verified/20200512_raspi_2.xz.sha256)                 |                                                                     |
|                |            |                     | [GPG-signed sha256sum](/verified/20200512_raspi_2.xz.sha256.asc)  |                                                                     |
|----------------|------------|---------------------|-------------------------------------------------------------------|---------------------------------------------------------------------|
| 2020.07.07     | 4          | 4 (4GB)             | [xz-compressed image](/verified/20200707_raspi_4.img.xz)          | Tested: Boots OK, HDMI (port #0 OK, port #1 upsdide down),          |
|                |            |                     | [sha256sum](/verified/20200707_raspi_4.xz.sha256)                 | wireless networking (wired networking should work), USB             |
|                |            |                     | [GPG-signed sha256sum](/verified/20200707_raspi_4.xz.sha256.asc)  | keyboard. Do note that the 8GB model will _not_ support USB!        |
|----------------|------------|---------------------|-------------------------------------------------------------------|---------------------------------------------------------------------|
