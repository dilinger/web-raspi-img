---
layout: page
title: Frequently Asked Questions (FAQs) and errata
date: 2020-07-07 01:06:23 -0500
permalink: /faq/
---

In this page you can find the [Frequently Asked Questions](#faqstart)
and the pending [errata on the image creation scripts](#errata).

## Frequently Asked Questions
<a name="faqstart" />

1. **Can you add package `foo` to the image? I think it's quite
   important!**
   
   Most likely, my answer will _no_. I am trying for the images to be
   as minimal and close to a base Debian install as possible; while
   most of us do use `console-tools` and `bash-completion` (two of the
   most common requests), it is not always required. Keep in mind the
   generated images should work for the most minimal Raspberry Pi
   model 1A (128MB RAM). And, all in all, it's just an `apt install`
   away for you anyway!
   
   If there is anything you _really_ need and cannot easily get
   done from the provided images, do contact me. But I don't want to
   change the images offered to everybody just because a feature is missing!

2. **How do I get wireless working?**

   Given we are shipping a minimal installation, I don't want to carry
   all the burden of _network-manager_, _wicd_ or the like. The
   easiest way is to create a `/etc/network/interfaces.d/wlan0` file
   with the settings for your network. I have found the following to
   be useful for –by far– most network settings I've encountered:
   
	    allow-hotplug wlan0
		iface wlan0 inet dhcp
		    wpa-ssid my-network-ssid
			wpa-psk s3kr3t_P4ss

   Given it contains the very sensitive `s3kr3t_P4ss`, I suggest you
   to set up this file to be readable only by the root
   (administrative) user:

		 # chown root:root /etc/network/interfaces.d/wlan0
		 # chmod 0600 /etc/network/interfaces.d/wlan0

   Of course, there might be other options you need — If that's the
   case, I suggest you look at the different options mentioned in the
   [WiFi/HowToUse page of the Debian
   Wiki](https://wiki.debian.org/WiFi/HowToUse).

3. **What about supporting the Raspberry Pi 4 family? You know, they
   are _sweet_ machines!**

   **[Update!]** As of 2020.07.07, we are finally shipping images for
   the Raspberry Pi 4 family! Please do note that, unlike all of the
   other images, these images _do_ contain what I would qualify as
   some dirtyness — The kernel (5.7.0) is taken from _unstable_, and
   they include a bit of firmware grabbed outside of Debian's
   repositories.
   
   Also, there are several varieties in the RPi4 family. As far as we
   have tested, the images work fine in the 2GB and 4GB models, but
   you will find **no USB support in the 8GB model** due mainly to a
   different memory layout. Of course, work is underway to fix this!

## Errata
<a name="errata" />

1. The images are set to boot the kernel with consoles in `/dev/tty0`
   (keyboard/HDMI) and `/dev/ttyS1` (serial port, configured to 115200
   bits per second). The correct console for the Raspberry pin headers
   is `/dev/ttyAMA0`.
   
   You can fix it by editing `cmdline.txt` in the first (FAT)
   partition, or in `/boot/firmware/cmdline.txt` if your system has
   already booted, and changing `ttyS1` for `ttyAMA1`.
